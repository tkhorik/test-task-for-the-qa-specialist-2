import java.io.*;
import java.util.ArrayList;

public class StreemImpl implements Streem {

    String fileName = FilenameGenerator.FILENAMEFORTEST;

    String line = null;

    public String readFromFile(String fileName) {
        ArrayList<String> list = new ArrayList<String>();
        try {
            byte[] buffer = new byte[1000];

            int total;
            try (FileInputStream inputStream = new FileInputStream(fileName)) {

                total = 0;
                int nRead = 0;
                while ((nRead = inputStream.read(buffer)) != -1) {
                    return (new String(buffer));
                }

                inputStream.close();
            }

            System.out.println("Read " + total + " bytes");
        } catch (FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + fileName + "'");
        }
        return fileName;
    }

/*    @Override
    public String readFromFile() throws FileNotFoundException {
        return null;
    }*/



    @Override
    public void writeIntoFile(String fileName) throws IOException {

        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileName), "utf-8"));
            writer.write("1,22,6,4,3");
            System.out.println("запись была произведена в файл " + fileName);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }finally {
            writer.close();
        }
    }

    @Override
    public String printToConsole() {
        return null;
    }


}