import java.util.Date;

public class FilenameGenerator {

    static String FILENAMEFORTEST;


    public String getFILENAMEFORTEST() {
        return FILENAMEFORTEST;
    }

    String setFILENAMEFORTEST(String FILENAMEFORTEST) {
        this.FILENAMEFORTEST = FILENAMEFORTEST;
        return FILENAMEFORTEST;
    }

    FilenameGenerator() {
        FILENAMEFORTEST = FILENAMEFORTEST;
    }

    String generateFileName() {
        String fileName1 = new StringBuilder().append("resul-file").append(new Date().toString())
                .append(".txt").toString();
        return fileName1.replaceAll("\\s+", "_").replaceAll(":", "-").trim();
    }
}