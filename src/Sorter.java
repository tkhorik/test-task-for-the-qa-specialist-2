import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static javax.script.ScriptEngine.FILENAME;

class Sorter extends StreemImpl {

    static String SORTEDSTRING = null;

    public static String getSORTEDSTRING() {
        return SORTEDSTRING;
    }

    private static void setSORTEDSTRING(String SORTEDSTRING) {
        Sorter.SORTEDSTRING = SORTEDSTRING;
    }

    public String getStringForSorter() {
        return stringForSorter;
    }

    public void setStringForSorter(String stringForSorter) {
        this.stringForSorter = stringForSorter;
    }

    private String stringForSorter = readFromFile(FilenameGenerator.FILENAMEFORTEST).replaceAll("[^0-9, ]", "");

    Sorter() {
    }

    String returnSortetItems() {
        List<String> items = Arrays.asList(stringForSorter.split(","));
        List<Integer> listIntegers = new ArrayList<Integer>(items.size());
        for (String current : items) {
            listIntegers.add(Integer.parseInt(current));
        }
        Collections.sort(listIntegers);
        System.out.println("\nascending " + listIntegers );
        listIntegers.sort(Collections.reverseOrder());
        String sortedtString = listIntegers.toString();
        System.out.println("\ndescending " + sortedtString );
        setSORTEDSTRING(sortedtString);

        return null;
    }
}
