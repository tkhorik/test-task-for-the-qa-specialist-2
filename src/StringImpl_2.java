import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.BaseStream;
import java.util.stream.Collectors;

public class StringImpl_2 implements Streem {

    private String FILENAMEFORTEST;

    public void setFILENAMEFORTEST(String FILENAMEFORTEST) {
        this.FILENAMEFORTEST = FILENAMEFORTEST;
    }

    public String getFILENAMEFORTEST() {
        return FILENAMEFORTEST;
    }

    @Override
    public String readFromFile(String fileName) {
        String result = null;
        try {
            result = Files.lines(Paths.get(fileName))
                    .parallel()
                    .map(String::trim)
                    .filter(line -> line.length() > 2)
                    .collect(Collectors.joining());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void writeIntoFile(String fileName) {

        Writer writer = null;
        Generator generator = new Generator();
        String stringForWriting = generator.generateRandomedDigits(20);

        try (PrintWriter out = new PrintWriter(fileName)) {
            out.println(stringForWriting);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            e.getMessage();
        }
    }

    @Override
    public String printToConsole() {
        return new Sorter().returnSortetItems();
    }

    public void writeIntoFile(String fileName, String stringForWriting) {
        try (PrintWriter out = new PrintWriter(fileName)) {
            out.println(stringForWriting);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            e.getMessage();
        }
        System.out.println("\n запись " + stringForWriting + " была произведена в файл " + fileName);
    }
}