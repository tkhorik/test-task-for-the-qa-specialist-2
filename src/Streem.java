import java.io.FileNotFoundException;
import java.io.IOException;

public interface Streem {

    String readFromFile(String fileName) throws FileNotFoundException;

    void writeIntoFile(String fileName) throws IOException;
    String printToConsole();
}
